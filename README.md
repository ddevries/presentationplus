This is a nice example project featuring Yeoman, Grunt, Bower, Karma and Jasmin and using AngularJs as the basis for js code. I shared it for the more experienced javascript programmers @emakina to look around. 

# presentation-plus
With all the shortcomings of AngularJS in mind, AngularJS has been deliberately chosen for this project. AngularJs is ideal for these small, "rapid prototype" kinda projects. While AngularJS is quite slow for large applications, it is fast enough for this application. It is also versatile enough to offer everything we need to meet the requirements quickly. The main reasons for choosing AngularJS are:

* Easy to set up an MVC pattern
* Two-way-databinding lets us focus on functionality instead of DOM-Manipulation
* AngularJS has protection agains XSS out of the box
* Official Yeoman generator makes setup even faster and reliable


This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.