'use strict';
(function(){
    
  var Slide = function (title, text, animation){
    return {
      title: title,
      text: text,
      transition : animation
    };
  }; 
  
  var Presentation = function (name){
    return {
      name: name,
      slides : [new Slide('','','')]
    };
  }; 

  /**
   * @ngdoc service
   * @name presentationPlusApp.PresentationService
   * @param localStorage
   * @description
   * # PresentationService
   * Service in the presentationPlusApp.
   */
  angular.module('presentationPlusApp')
    .value('localStorage', window.localStorage)
    .service('PresentationService',function (localStorage) {
       // Cache, to prevent parsing JSON on every access.
      var presentationsCache = [];

      //Get the presentations, not as (stored) JSON but as an object.
      var getPresentationsFromLocalStorage = function () {
        if (presentationsCache.length === 0 && typeof localStorage.presentations !== 'undefined') {
          presentationsCache = JSON.parse(localStorage.presentations);
        }
        return angular.copy(presentationsCache);
      };

      //Write presentations to localstorage as strigified json    
      var savePresentationsToLocalStorage = function () {
        localStorage.presentations = JSON.stringify(presentationsCache);
        return angular.copy(presentationsCache);
      };
      
      var getPresentations = function(success){
        return success(getPresentationsFromLocalStorage());
      };

      var createPresentation = function (name, index, success){
        var i = (typeof index === 'number' && index <= presentationsCache.length) ? index: 0;
        presentationsCache.splice(i +1, 0, new Presentation(name));
        return success(savePresentationsToLocalStorage());
      };

      var deletePresentation = function (presentationIndex, success) {
        presentationsCache.splice(presentationIndex, 1);
        return success(savePresentationsToLocalStorage());
      };

      var findPresentationByName = function(presentationName) {
        for (var i = 0; i < getPresentationsFromLocalStorage().length; i++) {
          if (presentationsCache[i].name === presentationName) {
            return presentationsCache[i];
          }
        }
        throw new Error('Couldn\'t find Presentation with name: ' + presentationName);
      };

      var getSlides = function(presentationName, success) {
        var presentation = findPresentationByName(presentationName);
        return success(angular.copy(presentation.slides));
      };
      
      var createSlide = function(presentationName, index, success) {
        var presentation = findPresentationByName(presentationName);
        var i = (typeof index === 'number' && index <= presentation.slides.length) ? index: 0;
        presentation.slides.splice(i +1, 0, new Slide('','',''));
        savePresentationsToLocalStorage();
        return success(angular.copy(presentation.slides));
      };
      
      var deleteSlide = function(presentationName, index, success) {
        var presentation = findPresentationByName(presentationName);
        presentation.slides.splice(index, 1);
        savePresentationsToLocalStorage();
        return success(angular.copy(presentation.slides));
      };
      
      var saveSlide = function(presentationName, index, slide, success) {
         var slideFromCache = findPresentationByName(presentationName).slides[index];
         slideFromCache.title = slide.title;
         slideFromCache.text = slide.text;
         slideFromCache.transition = slide.transition;
         savePresentationsToLocalStorage();
         return success(angular.copy(slideFromCache));
      };

      // return only public methods
      return {
        getPresentations: function(success){
          return getPresentations(success);
        },

        createPresentation: function(name, index, success){
          createPresentation(name, index, success);
        }, 

        deletePresentation: function(presentationIndex, success) {
          deletePresentation(presentationIndex, success);
        },

        getSlides : function(presentationName, success) {
          return getSlides(presentationName, success);
        },
        
        createSlide : function(presentationName, index, success) {
          createSlide(presentationName, index, success);
        },
        
        deleteSlide : function(presentationName, index, success) {
          deleteSlide(presentationName, index, success);
        },
        
        saveSlide: function (presentationName, index, slide, success) {
          saveSlide(presentationName, index, slide, success);
        }
      };
    });
})();