'use strict';

/**
 * @ngdoc function
 * @name presentationPlusApp.controller:MainCtrl
 * @param $scope
 * @param PresentationService
 * @description
 * # MainCtrl
 * Controller of the presentationPlusApp
 */
angular.module('presentationPlusApp')
  .controller('MainCtrl', function ($scope, PresentationService) {
    $scope.createPresentation = function(presentationName){
      PresentationService.createPresentation(presentationName, $scope.presentations.length, function(presentations){
        $scope.showCreate = false;
        $scope.presentationNameToAdd = '';
        $scope.presentations = presentations;
      });
    }; 
    
    $scope.deletePresentation = function(presentationIndex){
      PresentationService.deletePresentation(presentationIndex, function(presentations){
        $scope.presentations = presentations;
      });
    }; 
    
    PresentationService.getPresentations(function(presentations){
      $scope.presentations = presentations;
    });
  });
