'use strict';

/**
 * @ngdoc function
 * @name presentationPlusApp.controller:EditCtrl
 * @param $scope
 * @param $routeParams
 * @param $document
 * @param PresentationService
 * @description
 * # EditCtrl
 * Controller of the presentationPlusApp
 */
angular.module('presentationPlusApp')
  .controller('PresentationCtrl', function ($scope, $routeParams, $document, PresentationService) {
    var theDocument = $document[0];
  
    function launchFullScreen(element) {
      if (element.requestFullScreen) {
        element.requestFullScreen();
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
      } else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
      }
    }
    
    function cancelFullScreen() {
      if (theDocument.cancelFullScreen) {
        theDocument.cancelFullScreen();
      } else if (theDocument.mozCancelFullScreen) {
        theDocument.mozCancelFullScreen();
      } else if (theDocument.webkitCancelFullScreen) {
        theDocument.webkitCancelFullScreen();
      }
    }

    $scope.currentPresentation = $routeParams.presentation;
    $scope.fullScreenState = false;
    $scope.activeSlide = 0;
    $scope.transitionOptions = ['', 'fade' , 'bounce', 'lightspeed'];
    
    $scope.addSlide = function (index) {
      PresentationService.createSlide($scope.currentPresentation, index, function(slides){
        $scope.slides = slides;
      });
    };

    $scope.deleteSlide = function (index) {
      PresentationService.deleteSlide($scope.currentPresentation, index, function(slides){
        $scope.slides = slides;
      });
    };

    // kinda slow to save every presentation on change everytime, but hey.
    $scope.saveSlide = function (index, slide) {
      PresentationService.saveSlide($scope.currentPresentation, index, slide, function (savedSlide){
        slide = savedSlide;
      });
    };
        
    $scope.toggleFullScreen = function () {
      if ($scope.fullScreenState === false) {
        launchFullScreen(theDocument.documentElement);
      } else {
        cancelFullScreen();
      }
      $scope.fullScreenState = !$scope.fullScreenState;
    };
        
    $scope.nextSlide = function() {
      if($scope.fullScreenState){
        if($scope.activeSlide === $scope.slides.length -1) {
          cancelFullScreen();
          $scope.activeSlide = 0;
          $scope.fullScreenState = false;
        } else {
          $scope.activeSlide++;
        }
      }
    };
    
    PresentationService.getSlides($scope.currentPresentation, function(slides){
      $scope.slides = slides;
    });
    
    $scope.$watch('slides.length', function () {
      if ($scope.slides.length === 0) {
        $scope.addSlide();
      }
    });
    
    $scope.$on('$destroy', function() {
      cancelFullScreen();
      $scope.fullScreenState = false;
    });
  });
