'use strict';

/**
 * @ngdoc overview
 * @name presentationPlusApp
 * @param $routeProvider
 * @description
 * # presentationPlusApp
 *
 * Main module of the application.
 */
angular
  .module('presentationPlusApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/:presentation', {
        templateUrl: 'views/presentation.html',
        controller: 'PresentationCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function($rootScope){
    $rootScope.$on('$routeChangeStart', function(current, next){ 
      $rootScope.navigationDirection = next.$$route.keys.length > $rootScope.oldKeysLength ? 'forward' : 'backward'; 
      $rootScope.oldKeysLength = next.$$route.keys.length;
    });
  });
