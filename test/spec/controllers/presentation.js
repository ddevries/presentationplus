'use strict';

describe('Controller: PresentationCtrl', function () {

  // load the controller's module
  beforeEach(module('presentationPlusApp'));

  var PresentationCtrl,
    scope,
    routeParams = {},
    PresentationService,
    mockPresenationName = 'Mock Presentation';

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    var mockPresentation = {name: mockPresenationName, slides: [{title:'mock slide title', text:'moc slide text'},{title:'mock slide title2', text:'moc slide text2'}]};
    scope = $rootScope.$new();
    routeParams.presentation = mockPresenationName;
    PresentationService = {
      getSlides: function(presentationName ,success){
        return success(mockPresentation.slides);
      },
      createSlide: function(presentationName, index ,success){
        return success(mockPresentation.slides);
      },
      deleteSlide: function(presentationName, index ,success){
        return success(mockPresentation.slides);
      },
      saveSlide: function(presentationName, index, slide ,success){
        return success(mockPresentation.slides);
      }
    };
    
    spyOn(PresentationService, 'getSlides').and.callThrough();
    spyOn(PresentationService, 'createSlide').and.callThrough();
    spyOn(PresentationService, 'deleteSlide').and.callThrough();
    spyOn(PresentationService, 'saveSlide').and.callThrough();
    
    PresentationCtrl = $controller('PresentationCtrl', {
      $scope: scope,
      $routeParams: routeParams,
      PresentationService: PresentationService
    });
  }));

  it('should get slides on init', function () {
    expect(PresentationService.getSlides).toHaveBeenCalledWith(mockPresenationName, jasmine.any(Function));
    expect(scope.slides.length).toBe(2);
  });
  
  it('should add a new slide at index', function () {
    scope.addSlide(0);
    expect(PresentationService.createSlide).toHaveBeenCalledWith(mockPresenationName,0 , jasmine.any(Function));
  });
  
  it('should delete slide at index', function () {
    scope.deleteSlide(0);
    expect(PresentationService.deleteSlide).toHaveBeenCalledWith(mockPresenationName,0 , jasmine.any(Function));
  });
  
  it('should create new slide when all slides are removed', function () {
    scope.slides.splice(1);
    scope.$apply();
    expect(PresentationService.createSlide).not.toHaveBeenCalled();

    scope.slides = [];
    scope.$apply();
    expect(PresentationService.createSlide).toHaveBeenCalledWith(mockPresenationName, undefined, jasmine.any(Function));
  });
       
  it('should save slide', function () {
    scope.saveSlide(0, scope.slides[0]);
    expect(PresentationService.saveSlide).toHaveBeenCalled();
  });
  
  it('should toggle fullscreen', function () {
    expect(scope.fullScreenState).toBe(false);
    scope.toggleFullScreen();
    expect(scope.fullScreenState).toBe(true);
    scope.toggleFullScreen();
    expect(scope.fullScreenState).toBe(false);
  });
  
  it('should exit fullscreen on scope.$destroy()', function () {
    expect(scope.fullScreenState).toBe(false);
    scope.toggleFullScreen();
    expect(scope.fullScreenState).toBe(true);
    scope.$destroy();
    expect(scope.fullScreenState).toBe(false);

  });
  
  it('should advance through slides and exit fullscreen after last', function () {
    expect(scope.activeSlide).toBe(0);
    scope.nextSlide();
    expect(scope.activeSlide).toBe(0);
    scope.toggleFullScreen();
    expect(scope.fullScreenState).toBe(true);
    expect(scope.activeSlide).toBe(0);
    scope.nextSlide();
    expect(scope.fullScreenState).toBe(true);
    expect(scope.activeSlide).toBe(1);
    scope.nextSlide();
    expect(scope.fullScreenState).toBe(false);
    expect(scope.activeSlide).toBe(0);
  });
});
