'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('presentationPlusApp'));

  var MainCtrl,
    scope,
    PresentationService,
    mockPresentations = [{name: 'Mock Presentation', slides: []}];

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PresentationService = {
      getPresentations: function(success){ 
        return success(mockPresentations);
      },
      createPresentation:function(presentationName, index, success) { 
        return success(mockPresentations);
      },
      deletePresentation:function(index, success) {  
        return success(mockPresentations);
      }
    };
    
    spyOn(PresentationService, 'getPresentations').and.callThrough();
    spyOn(PresentationService, 'createPresentation').and.callThrough();
    spyOn(PresentationService, 'deletePresentation').and.callThrough();
    
    MainCtrl = $controller('MainCtrl', {
      $scope: scope,
      PresentationService: PresentationService
    });
  }));

  it('should get presentations on init', function () {
    expect(PresentationService.getPresentations).toHaveBeenCalled();
    expect(scope.presentations.length).toBe(1);
  });
  
  it('should create new Presentation with name', function () {
    scope.createPresentation('New Presentation');
    expect(PresentationService.createPresentation).toHaveBeenCalledWith('New Presentation', 1, jasmine.any(Function));
    expect(scope.presentationNameToAdd).toBe('');
  });
  
  it('should delete presentation with index', function () {
    scope.deletePresentation(0);
    expect(PresentationService.deletePresentation).toHaveBeenCalledWith(0, jasmine.any(Function));
  }); 
});
