'use strict';

describe('Service: PresentationService', function () {

  // instantiate service
  var PresentationService,
    localStorage = window.localStorage,
    mockLocalPresentations = [{name: 'mockPresentation', slides: [{title: '', text: '', animation: ''}]}],
    mockJsonString = '[{"name":"mockPresentation","slides":[{"title":"","text":"","animation":""}]}]';


  beforeEach(function () {
    module('presentationPlusApp');

    localStorage.presentations = mockJsonString;

    inject(function (_PresentationService_) {
      PresentationService = _PresentationService_;
    });
  });

  it('should read from localStorage', function () {
    var presentations;
    PresentationService.getPresentations(function(retreivedPresentations){
      presentations = retreivedPresentations;
    });
    expect(presentations).toEqual(mockLocalPresentations);
  });

  it('should create, delete and cache presentations', function () {
    var presentations;
    PresentationService.getPresentations(function(retreivedPresentations){
      presentations = retreivedPresentations;
    });
    
    PresentationService.createPresentation('second Presentation', 0, function(){}); 
    PresentationService.createPresentation('third Presentation', null, function(){}); 
    PresentationService.createPresentation('forth Presentation', 7, function(){});   
    PresentationService.createPresentation('fifth Presentation', 2, function(retreivedPresentations){
      presentations = retreivedPresentations;
    });
    
    expect(presentations[1].name).toEqual('forth Presentation');
    expect(presentations[2].name).toEqual('third Presentation');
    expect(presentations[3].name).toEqual('fifth Presentation');
    
    localStorage.presentations = '';
    
    PresentationService.deletePresentation(4 , function(){});
    PresentationService.deletePresentation(3 , function(){});
    PresentationService.deletePresentation(2 , function(){});
    PresentationService.deletePresentation(1 , function(retreivedPresentations){
      presentations = retreivedPresentations;
    });
    
    expect(presentations).toEqual(mockLocalPresentations);
    expect(localStorage.presentations).toEqual(mockJsonString);
  });

  it('should throw exeption when presentation is not found', function () {
    expect(function () {
      PresentationService.getSlides('Unknown Presentation');
    }).toThrow(new Error('Couldn\'t find Presentation with name: Unknown Presentation'));
  });

  it('should create and delete slides', function () {
    var slides;
    var mockSlide = {title: 'mockSlideTitle', text: 'mockSlideText', transition: 'mockSlideTransition'};
    
    PresentationService.getSlides('mockPresentation', function(retreivedSlides){
      slides = retreivedSlides;
    });
    
    expect(slides).toEqual(mockLocalPresentations[0].slides);
    
    PresentationService.createSlide('mockPresentation', 1, function(retreivedSlides){
      slides = retreivedSlides;
    });
    
    PresentationService.saveSlide('mockPresentation', 1, mockSlide, function(retreivedSlide){
      slides[1] = retreivedSlide;
    });
    
    expect(slides[1].title).toEqual('mockSlideTitle');
    
    PresentationService.deleteSlide('mockPresentation', 1, function (retreivedSlides){
      slides = retreivedSlides;
    });
    
    expect(slides).toEqual(mockLocalPresentations[0].slides);
  });
});
